package ru.android.weather.repositories;

import android.app.Application;
import java.util.List;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.android.weather.local.persistence.CitiesRoomDatabase;
import ru.android.weather.local.persistence.City;
import ru.android.weather.local.persistence.CityDao;

public class CityRepository {

    private CityDao cityDao;

    public CityRepository(Application application) {
        CitiesRoomDatabase db = CitiesRoomDatabase.getDatabase(application);
        cityDao = db.cityDao();
    }

    public Flowable<List<City>> getAllCities() {
        return cityDao.getCities()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public void insert(City city) {
        CitiesRoomDatabase.databaseWriteExecutor.execute(() -> {
            cityDao.insert(city);
        });
    }

    public void delete(City city) {
        CitiesRoomDatabase.databaseWriteExecutor.execute(() -> {
            cityDao.delete(city);
        });
    }
}
