package ru.android.weather.viewmodel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import java.util.List;
import io.reactivex.Flowable;
import ru.android.weather.local.persistence.City;
import ru.android.weather.repositories.CityRepository;

public class CityViewModel extends AndroidViewModel {

    private CityRepository repository;

    public CityViewModel(Application application) {
        super(application);
        repository = new CityRepository(application);
    }

    public Flowable<List<City>> getAllCities() {
        return repository.getAllCities();
    }

    public void insert(City city) {
        repository.insert(city);
    }

    public void delete(City city) {
        repository.delete(city);
    }
}
