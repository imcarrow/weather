package ru.android.weather.ui;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import ru.android.weather.BuildConfig;
import ru.android.weather.R;

public class AddCityActivity extends AppCompatActivity implements OnClickListener {

    private EditText editTitleView;
    private EditText editValueView;
    private EditText editKeyView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_city);
        editTitleView = findViewById(R.id.edit_title);
        editValueView = findViewById(R.id.edit_value);
        editKeyView = findViewById(R.id.edit_key);

        final Button button = findViewById(R.id.button_save);
        button.setOnClickListener(this);
    }

    public void onClick(View v) {
        Intent replyIntent = new Intent();
        if (TextUtils.isEmpty(editTitleView.getText())) {
            setResult(RESULT_CANCELED, replyIntent);
        } else {
            String title = editTitleView.getText().toString();
            String value = editValueView.getText().toString();
            String key = editKeyView.getText().toString();
            replyIntent.putExtra(BuildConfig.CITY_TITLE, title);
            replyIntent.putExtra(BuildConfig.CITY_VALUE, value);
            replyIntent.putExtra(BuildConfig.CITY_KEY, key);
            setResult(RESULT_OK, replyIntent);
        }
        finish();
    }
}