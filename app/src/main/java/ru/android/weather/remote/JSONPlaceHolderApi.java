package ru.android.weather.remote;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.android.weather.remote.models.DayWeather;

public interface JSONPlaceHolderApi {

    @GET("weather?units=metric")
    Single<DayWeather> getDayWeather(
            @Query("q") String city,
            @Query("APPID") String apiId);
}
