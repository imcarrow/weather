package ru.android.weather.local.persistence;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import java.util.List;
import io.reactivex.Flowable;

@Dao
public interface CityDao {

    @Query("SELECT * from cities ORDER BY value ASC")
    Flowable<List<City>> getCities();

    @Delete
    void delete(City city);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(City city);
}
