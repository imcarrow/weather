package ru.android.weather.repositories;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.android.weather.BuildConfig;
import ru.android.weather.remote.JSONPlaceHolderApi;
import ru.android.weather.remote.models.DayWeather;

public class DayWeatherRepository {

    JSONPlaceHolderApi jsonApi;

    public DayWeatherRepository() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder client = new OkHttpClient.Builder().addInterceptor(interceptor);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client.build())
                .build();

        jsonApi = retrofit.create(JSONPlaceHolderApi.class);
    }

    public Single<DayWeather> getDayWeather(String cityName, String apiId) {
        return jsonApi.getDayWeather(cityName, apiId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}