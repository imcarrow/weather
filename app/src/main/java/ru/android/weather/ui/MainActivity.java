package ru.android.weather.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View.OnClickListener;
import android.view.View;
import android.widget.Toast;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import java.util.List;
import io.reactivex.functions.Consumer;
import ru.android.weather.BuildConfig;
import ru.android.weather.adapters.CityListAdapter;
import ru.android.weather.R;
import ru.android.weather.local.persistence.City;
import ru.android.weather.viewmodel.CityViewModel;

public class MainActivity extends AppCompatActivity implements OnClickListener, CityListAdapter.OnItemClickListener {

    FloatingActionButton addCityBtn;

    CityViewModel cityViewModel;

    List<City> cities;

    public static final int NEW_CITY_ACTIVITY_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        CityListAdapter adapter = new CityListAdapter(this, this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        cityViewModel = new ViewModelProvider(this).get(CityViewModel.class);

        cityViewModel.getAllCities().subscribe(new Consumer<List<City>>() {
            @Override
            public void accept(List<City> allCities) throws Exception {
                cities = allCities;
                adapter.setCities(allCities);
            };
        });

        addCityBtn = findViewById(R.id.addCity);
        addCityBtn.setOnClickListener(this);
    }

    public void onClick(View v) {
        Intent intent = new Intent(this, AddCityActivity.class);
        startActivityForResult(intent, NEW_CITY_ACTIVITY_REQUEST_CODE);
    }

    public void onItemClick(int position, View view) {
        if(view.getId() == R.id.textView) {
            Intent intent = new Intent(this, DetailActivity.class);
            intent.putExtra(BuildConfig.CITY_TITLE, String.valueOf(cities.get(position).getTitle()));
            intent.putExtra(BuildConfig.CITY_VALUE, String.valueOf(cities.get(position).getValue()));
            intent.putExtra(BuildConfig.CITY_KEY, String.valueOf(cities.get(position).getKey()));
            startActivity(intent);
        }
        if(view.getId() == R.id.deleteCity) {
            City city = cities.get(position);
            cityViewModel.delete(city);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == NEW_CITY_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            String label = data.getStringExtra(BuildConfig.CITY_TITLE);
            String value = data.getStringExtra(BuildConfig.CITY_VALUE);
            String key = data.getStringExtra(BuildConfig.CITY_KEY);
            City city = new City(label, value, key);
            cityViewModel.insert(city);
        } else {
            Resources res = getResources();;
            Toast.makeText(
                    getApplicationContext(),
                    res.getString(R.string.empty_not_saved),
                    Toast.LENGTH_LONG).show();
        }
    }
}