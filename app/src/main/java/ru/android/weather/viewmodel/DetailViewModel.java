package ru.android.weather.viewmodel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import io.reactivex.Single;
import ru.android.weather.remote.models.DayWeather;
import ru.android.weather.repositories.DayWeatherRepository;

public class DetailViewModel extends AndroidViewModel {
    private DayWeatherRepository dayWeatherRepository;

    public DetailViewModel(Application application) {
        super(application);
        dayWeatherRepository= new DayWeatherRepository();
    }

    public Single<DayWeather> getDayWeather(String cityName, String apiKey) {
        return dayWeatherRepository.getDayWeather(cityName, apiKey);
    }
}
