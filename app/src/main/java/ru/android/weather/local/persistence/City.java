package ru.android.weather.local.persistence;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Cities")
public class City {
    @PrimaryKey(autoGenerate = true)
    public int id;
    private String title;
    private String value;
    private String key;

    public City(String title, String value, String key) {
        this.title = title;
        this.value = value;
        this.key = key;
    }

    public String getTitle() {
        return title;
    }

    public String getValue() {
        return value;
    }

    public String getKey() {
        return key;
    }
}
