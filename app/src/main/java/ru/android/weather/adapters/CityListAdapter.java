package ru.android.weather.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView.Adapter;

import java.util.List;

import ru.android.weather.R;
import ru.android.weather.local.persistence.City;
import ru.android.weather.adapters.viewholders.CityViewHolder;

public class CityListAdapter extends Adapter<CityViewHolder> {

    public interface OnItemClickListener {
        void onItemClick(int position, View view);
    }

    private List<City> cities;

    private final LayoutInflater inflater;
    private OnItemClickListener listener;

    public CityListAdapter(Context context, OnItemClickListener listener) {
        inflater = LayoutInflater.from(context);
        this.listener = listener;
    }

    @Override
    public CityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.recyclerview_item, parent, false);
        return new CityViewHolder(itemView, listener);
    }

    @Override
    public void onBindViewHolder(CityViewHolder holder, int position) {
        if (this.cities != null) {
            City current = this.cities.get(position);
            holder.cityItemView.setText(current.getTitle());
        } else {
            holder.cityItemView.setText(R.string.no_city);
        }
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (this.cities != null)
            return this.cities.size();
        else return 0;
    }
}


