package ru.android.weather.local.persistence;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.reactivex.functions.Consumer;

@Database(entities = {City.class}, version = 1, exportSchema = false)
public abstract class CitiesRoomDatabase extends RoomDatabase {

    public abstract CityDao cityDao();

    private static volatile CitiesRoomDatabase INSTANCE;

    private static final int NUMBER_OF_THREADS = 4;

    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public static CitiesRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (CitiesRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            CitiesRoomDatabase.class, "word_database")
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback() {
        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
            databaseWriteExecutor.execute(() -> {
                CityDao dao = INSTANCE.cityDao();
                dao.getCities().subscribe(new Consumer<List<City>>() {
                    @Override
                    public void accept(List<City> allCities) throws Exception {
                        if (allCities.isEmpty()) {
                            City cityPerm = new City("Пермь", "Perm", "perm");
                            dao.insert(cityPerm);
                        }
                    };
                });
            });
        };
    };
}
