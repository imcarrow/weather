package ru.android.weather.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import java.text.DateFormat;
import java.util.Date;
import ru.android.weather.BuildConfig;
import ru.android.weather.R;
import ru.android.weather.remote.models.Main;
import ru.android.weather.remote.models.Weather;
import ru.android.weather.remote.models.DayWeather;
import ru.android.weather.viewmodel.DetailViewModel;

public class DetailActivity extends AppCompatActivity {

    TextView cityField;
    TextView updatedField;
    TextView detailsField;
    TextView currentTemperatureField;
    ImageView weatherIcon;

    private Intent intent;

    private String cityValue;

    DetailViewModel detailViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        cityField = (TextView)findViewById(R.id.city_field);
        weatherIcon = (ImageView)findViewById(R.id.weather_icon);
        updatedField = (TextView)findViewById(R.id.updated_field);
        detailsField = (TextView)findViewById(R.id.details_field);
        currentTemperatureField = (TextView)findViewById(R.id.current_temperature_field);

        intent = getIntent();
        cityValue = intent.getStringExtra(BuildConfig.CITY_VALUE);
        detailViewModel = new ViewModelProvider(this).get(DetailViewModel.class);
        detailViewModel.getDayWeather(cityValue, BuildConfig.API_KEY)
                .subscribe(this::onSuccessGetDayWeather, this::onErrorGetDayWeather);
    }

    private void onSuccessGetDayWeather(DayWeather dayWeather) {
        Weather weather = dayWeather.getWeather().get(0);
        Main main = dayWeather.getMain();
        Resources res = getResources();
        cityField.setText(dayWeather.getCityName());
        detailsField.setText(
                weather.getDescription() +
                        "\n" + res.getString(R.string.humidity) + ": " +
                        String.valueOf(main.getHumidity()) + " " + res.getString(R.string.percent) +
                        "\n" + res.getString(R.string.pressure) + ": " +
                        String.valueOf(main.getPressure()) + " "+ res.getString(R.string.val_pressure));
        currentTemperatureField.setText(String.format("%.2f", main.getTemp()) + " " + res.getString(R.string.сelsius));
        DateFormat df = DateFormat.getDateTimeInstance();
        String updatedOn = df.format(new Date(dayWeather.getDt() * 1000));
        updatedField.setText(res.getString(R.string.last_update) + ": "  + updatedOn);
        String url = String.format(BuildConfig.IMAGE_URL, weather.getIcon());
        Picasso.get().load(url).into(weatherIcon);
    }

    private void onErrorGetDayWeather(Throwable throwable) {
        cityField.setText(throwable.getMessage());
        System.out.println(throwable);
    }

}