package ru.android.weather.adapters.viewholders;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import ru.android.weather.R;
import ru.android.weather.adapters.CityListAdapter;

public class CityViewHolder extends ViewHolder implements OnClickListener {

    public final TextView cityItemView;
    public final FloatingActionButton deleteCity;

    private CityListAdapter.OnItemClickListener listener;

    public CityViewHolder(View itemView, CityListAdapter.OnItemClickListener listener) {
        super(itemView);
        cityItemView = itemView.findViewById(R.id.textView);
        deleteCity = itemView.findViewById(R.id.deleteCity);
        this.listener = listener;
        cityItemView.setOnClickListener(this);
        deleteCity.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int position = getAdapterPosition();
        if(listener != null && position != RecyclerView.NO_POSITION){
            listener.onItemClick(position , v);
        }
    }
}