package ru.android.weather.remote.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class DayWeather {

    @SerializedName("name")
    @Expose
    private String cityName;

    @SerializedName("dt")
    @Expose
    private Long dt;

    @SerializedName("weather")
    @Expose
    private List<Weather> weather;

    @SerializedName("main")
    @Expose
    private Main main;

    @SerializedName("sys")
    @Expose
    private Sys sys;

    public DayWeather() {
        weather = new ArrayList<Weather>();
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public void setDt(Long dt) {
        this.dt = dt;
    }

    public void setWeather(List<Weather> weather) {
        this.weather = weather;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public String getCityName() {
        return cityName;
    }

    public Long getDt() {
        return dt;
    }

    public List<Weather> getWeather() {
        return weather;
    }

    public Main getMain() {
        return main;
    }

    public Sys getSys() {
        return sys;
    }

    public void setSys(Sys sys) {
        this.sys = sys;
    }
}
